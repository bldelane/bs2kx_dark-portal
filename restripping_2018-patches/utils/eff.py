from collections import namedtuple
import math

EfficiencyResult = namedtuple("EfficiencyResult", ["n", "s"])


def eff_ratio(n, denom):
    if denom == 0:
        return EfficiencyResult(
            None, None
        )  # return a namedtuple with None values if denom is 0

    efficiency = n / denom
    error = math.sqrt((efficiency * (1 - efficiency)) / denom)

    return EfficiencyResult(efficiency, error)
