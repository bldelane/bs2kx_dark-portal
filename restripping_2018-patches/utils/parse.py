"""Utility to parse parameters into the corresponding branch aliases."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

from collections import namedtuple
from typing import Any


def branch_alias_factory(expr) -> Any:
    """Convert an expression into a branch alias, as set by DaVinci/TupleTools.

    Parameters
    ----------
    expr : str
        Branch expression.

    Returns
    -------
    str
        Branch alias.
    """
    # book a container with fields
    # - alias: the branch alias in the TTree
    # - op: the operation to be performed on the branch; a pandas-parseable string
    CutVar = namedtuple("CutVar", ["alias", "op"])

    # separate particle and observable
    try:
        particle, var = expr.split("_", 1)
    except ValueError:
        print("Invalid expression. Expected format: 'particle_var'")

    # return now thee updated alias and relevant expression
    match var:
        case "PT_MIN":
            return CutVar(f"{particle}_PT", f"{particle}_PT > ")
        case "P_MIN":
            return CutVar(f"{particle}_P", f"{particle}_P > ")
        case "BPVLTIME_MIN":
            return CutVar(f"{particle}_BPVLTIME", f"{particle}_BPVLTIME > ")
        case "VCHI2DOF_MAX":
            return CutVar(
                f"{particle}_ENDVERTEX_CHI2", f"{particle}_ENDVERTEX_CHI2 < "
            )  # NOTE: here we don't normalise by NDOF. For B2K*X, we have two tracks so should be the same. A two-track vertex has one degree of freedom. A three-track vertex has three degrees of freedom.
        case "BPVVDCHI2_MIN":
            return CutVar(
                f"{particle}_BPVVDCHI2", f"{particle}_BPVVDCHI2 > "
            )  # chi2 -distance from the related PV
        case "BPVIPCHI2_MAX":
            return CutVar(
                f"{particle}_IPCHI2_OWNPV", f"{particle}_IPCHI2_OWNPV < "
            )  # chi2 -distance from the related PV
        case "HAD_MINIPCHI2_MIN":
            return CutVar(
                f"{particle}_IPCHI2_OWNPV", f"{particle}_IPCHI2_OWNPV > "
            )  # FIXME: check
        case "MIPCHI2DV_MIN":
            return CutVar(
                f"{particle}_IPCHI2_OWNPV", f"{particle}_IPCHI2_OWNPV > "
            )  # FIXME: check
        case "TRGHP_MAX":
            return CutVar(
                f"{particle}_TRACK_GhostProb", f"{particle}_TRACK_GhostProb < "
            )
        case "PROBNNpi_MIN":
            return CutVar(f"{particle}_ProbNNpi", f"{particle}_ProbNNpi > ")
        case "TRCHI2DOF_MAX":
            return CutVar(f"{particle}_TRACK_CHI2NDOF", f"{particle}_TRACK_CHI2NDOF < ")
        case "CL_MIN":
            return CutVar(f"{particle}_CL", f"{particle}_CL > ")
        case _:
            print(f"Unaccounted observable: {var}")


def tuplekey_factory(fileid) -> str | list[str]:
    """Return the appropriate tuple key(s) for a given filename.

    Parameters
    ----------
    fileid : str
        Identifier in the file name.

    Returns
    -------
    str | list[str]
        Tuple key(s).
    """
    if isinstance(fileid, str):
        probed_path = fileid
    if isinstance(fileid, list):
        probed_path = fileid[0]

    match probed_path:
        case fileid if "etapipi" in fileid and "111044" in fileid:
            return ["BdKstEtaPiPi"]
        case fileid if "etapipi" in fileid and "121034" in fileid:
            return ["BuKEtaPiPi"]
        case fileid if "pipipi" in fileid:
            return ["BdKstPi0PiPiR", "BdKstpi0PiPiM"]


def build_stripping_sel_dict(sel_dict: dict, channel: str) -> dict:
    """Build a dictionary of stripping selections from a dictionary of stripping selections."""
    new_dict = {
        f"{subkey}_{key}": value
        for subkey, subdict in sel_dict[channel].items()
        for key, value in subdict.items()
    }

    return new_dict


def generate_query_string(my_dict: dict) -> str:
    """Generate a query string from a dictionary."""
    return " & ".join(f"(abs({k}) == {v})" for k, v in my_dict.items())
