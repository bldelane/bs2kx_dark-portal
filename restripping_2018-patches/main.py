"""Main script to study the efficiency of the cuts applied for the ALP and dark-scalar lines added/amended in the 2018-patches LHCb campaign."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"
__version__ = "1.0.0"

import pandas as pd
import numpy as np
import json
import uproot
import awkward as ak
import os
from pathlib import Path
from utils import *  # FIXME: this is bad practice
from utils import (
    branch_alias_factory,
    build_stripping_sel_dict,
    generate_query_string,
    get_files,
    process_root_files,
    simple_ax,
    plot_data,
    hist_step_fill,
    eff_ratio,
    in_dict,
    generate_query_string,
    tuplekey_factory,
)
import matplotlib.pyplot as plt
import argparse
import pandas as pd


def generate_latex_snippet(plot_path: str, to_file: bool = False) -> str | None:
    """Generate LaTeX code to display the plots in the plot_path directory."""
    # Find all the .pdf files in the plot_path directory
    pdf_files = [f for f in os.listdir(plot_path) if f.endswith(".pdf")]

    # Initialize LaTeX code
    latex_code = ""

    # Generate LaTeX code
    for i in range(0, len(pdf_files), 2):
        latex_code += r"\begin{figure}[htbp]" "\n"

        for j in range(2):
            if i + j < len(pdf_files):
                file = pdf_files[i + j]
                latex_code += r"  \begin{minipage}{0.45\textwidth}" "\n"
                latex_code += r"    \centering" "\n"
                latex_code += (
                    r"    \includegraphics[width=\textwidth]{"
                    + f"{plot_path}/{file}"
                    + r"}"
                    "\n"
                )
                latex_code += (
                    r"    \caption{\footnotesize{"
                    + f"Normalised histograms of the full MC sample (black) and truth-matched signal (blue). The red dashed line illustrates the stripping selection, with -10 indicating a dummy cut not implemented in the module, employed just to visualize the distributions. The exclusive efficiency for the cut, evaluated on the truth-matched sample, is also reported."
                    + r"}}"
                    "\n"
                )
                latex_code += r"  \end{minipage}" "\n"
                if j == 0:
                    latex_code += r"  \hfill" "\n"

        latex_code += r"\end{figure}" "\n"
        latex_code += "\n"

    # If to_file is True, write the latex code to file
    if to_file:
        with open(os.path.join(plot_path, "figs.tex"), "w") as f:
            f.write(latex_code)

    return latex_code


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input",
        required=True,
        help="Path to file(s) of interest",
        action="append",
    )
    args = parser.parse_args()

    # based on args, parse the relevant channel
    _channels = tuplekey_factory(fileid=args.input)

    for CHANNEL in _channels:
        print(f">>>Inspecting the channel: {CHANNEL}")

        # load the json file containing the cuts and
        tm_dict = in_dict(
            "/work/submit/blaised/bs2kx_dark-portal/restripping_2018-patches/truthmatching.json"
        )[CHANNEL]

        sel_dict = build_stripping_sel_dict(
            sel_dict=in_dict(
                "/work/submit/blaised/bs2kx_dark-portal/restripping_2018-patches/cutvars.json"
            ),
            channel=CHANNEL,
        )

        # compile the relevant list of branches
        VARCUTS = list(
            map(branch_alias_factory, sel_dict.keys())
        )  # NOTE: takes the form CutVar(alias, op)
        assert None not in VARCUTS, "Invalid branch alias"

        # raw data, no truthmatching
        data = process_root_files(
            file_list=args.input,
            key=CHANNEL,
            treename="DecayTree",
            selection=None,
            max_entries=-1,
            library="ak",
            branches=[v.alias for v in VARCUTS] + list(tm_dict.keys()) + ["B_P"],
        )
        # convert to pandas -> easy management of all the cuts
        data = ak.to_pandas(data)

        # parse the relevant truthmatching cuts
        tm = generate_query_string(tm_dict)

        # add the truthmatching cuts to identify the signal
        sig = data.query(tm)

        # loop over the cuts and plot the distributions, reporting the exclusive efficiency
        _plot_path = f"plots/{CHANNEL}"

        # to generate a latex efficiency table
        eff_dict = {}

        for var, cut_val in sel_dict.items():
            # translate to the TTrees branch alias
            v = branch_alias_factory(var)
            print(f"Inspecting the selection pair: {v.op}{cut_val}")

            # set the range of the histogram
            _range = (data[v.alias].min(), data[v.alias].max())
            if "IPCHI2" in v.alias:
                _range = (0, 60)
            if "LTIME" in v.alias:
                _range = (0, 0.10)
            plt_conf = {
                "bins": 100,
                "range": _range,
                "norm": True,
            }

            Path(_plot_path).mkdir(parents=True, exist_ok=True)

            # book suitable title
            match CHANNEL:
                case "BdKstEtaPiPi":
                    title = r"$B \to K^{*} X (\to \eta\pi\pi)$"
                case "BdKstpi0PiPiM":
                    title = r"$B \to K^{*} X (\to \pi\pi\pi_{M}^0)$"
                case "BdKstPi0PiPiR":
                    title = r"$B \to K^{*} X (\to \pi\pi\pi_{R}^0)$"
                case "BuKEtaPiPi":
                    title = r"$B \to K X (\to \eta\pi\pi)$"

            fig, ax = simple_ax(title=title)
            plot_data(data=data[v.alias], ax=ax, **plt_conf, label="All Data")
            hist_step_fill(data=sig[v.alias], ax=ax, **plt_conf, label="Signal")

            # display cut value
            ax.axvline(
                cut_val,
                color="r",
                linestyle="--",
                lw=0.5,
                label=f"Cut Value: {cut_val}",
            )

            # tally efficiency of selection on truthmatched signal
            eff = eff_ratio(len(sig.query(f"{v.op}{cut_val}")), len(sig))
            # log the efficiency
            eff_dict[f"{v.op}{cut_val}"] = (
                r"$\varepsilon_{excl} = $" + f" {eff.n*100.:.2f} $\pm$ {eff.s*100.:.2f}"
            )

            # generate the plot
            ax.plot(
                [],
                [],
                " ",
                label=r"$\varepsilon_{excl} = $"
                + f" {eff.n*100.:.2f} $\pm$ {eff.s*100.:.2f}",
                alpha=0,
            )
            # cosmetics and save (+close for memory)
            ax.legend()
            ax.set_xlabel(v.alias.replace("_", " "))
            plt.savefig(f"{_plot_path}/{v.alias}.pdf")
            ax.set_yscale("log")
            plt.savefig(f"{_plot_path}/{v.alias}_log.pdf")
            plt.close()

        # labelling for tex - remove _
        eff_dict = {
            key.replace("_", " ").replace(">", "$>$").replace("<", "$<$"): value
            for key, value in eff_dict.items()
        }

        # generate the latex table
        tex_eff_table = pd.DataFrame(
            list(eff_dict.items()), columns=["Cut", "Efficiency"]
        ).to_latex(index=False, escape=False)

        # write to .tex efficiency table file
        with open(f"{_plot_path}/effs.tex", "w") as f:
            f.write(tex_eff_table)

        # generate the latex snippet to include all the plots
        generate_latex_snippet(_plot_path, to_file=True)
